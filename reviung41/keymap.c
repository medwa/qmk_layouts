/* Copyright 2020 gtips
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum layer_names {
    _BASE,
    _NUM,
    _SYM,
    _NAV
};

#define NUM MO(_NUM)
#define SYM MO(_SYM)
#define NAV MO(_NAV)
#define CTL_ESC LCTL_T(KC_ESC)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_BASE] = LAYOUT_reviung41(
    KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,      KC_T,             KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_BSPC,
    CTL_ESC,  KC_A,     KC_S,     KC_D,     KC_F,      KC_G,             KC_H,     KC_J,     KC_K,     KC_L,     KC_SCLN,  KC_QUOT,
    KC_LSFT,  KC_Z,     KC_X,     KC_C,     KC_V,      KC_B,             KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_ENT,
                                            KC_LGUI,   NUM,    KC_SPC,   SYM,      KC_RALT
  ),

  [_NUM] = LAYOUT_reviung41(
    _______,  KC_1,     KC_2,     KC_3,     KC_4,      KC_5,               KC_6,     KC_7,    KC_8,    KC_9,    KC_0,      _______,
    _______,  KC_F1,    KC_F2,    KC_F3,    KC_F4,     KC_F5,              KC_F6,    KC_4,    KC_5,    KC_6,    KC_PPLS,   KC_PAST,
    _______,  KC_F7,    KC_F8,    KC_F9,    KC_F10,    KC_F11,             KC_F12,   KC_1,    KC_2,    KC_3,    KC_PMNS,   KC_PSLS,
                                            XXXXXXX,   _______,  KC_PENT,  KC_0,     KC_DOT
  ),

  [_SYM] = LAYOUT_reviung41(
    _______,  KC_EXLM,  KC_AT,    KC_HASH,  KC_DLR,     KC_PERC,           KC_CIRC,  KC_AMPR,  KC_ASTR,  KC_LPRN,  KC_RPRN,  _______,
    _______,  KC_INS,   KC_HOME,  KC_PGUP,  KC_PSCR,    XXXXXXX,           KC_TILD,  KC_MINS,  KC_PLUS,  KC_LBRC,  KC_RBRC,  KC_PIPE,
    _______,  KC_PAUS,  KC_END,   KC_PGDN,  S(KC_PSCR), XXXXXXX,           KC_GRV,   KC_UNDS,  KC_EQL,   KC_LCBR,  KC_RCBR,  KC_BSLS,
                                            _______,    _______,  KC_SPC,   _______,  XXXXXXX
  ),

  [_NAV] = LAYOUT_reviung41(
    _______,   RGB_SAI, RGB_SAD,  RGB_HUI,  RGB_HUD,   RGB_TOG,            XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_DEL,
    _______,   KC_EJCT, KC_MPLY,  KC_MPRV,  KC_MNXT,   RGB_MOD,            KC_LEFT,  KC_DOWN,  KC_UP,    KC_RGHT,  XXXXXXX,  KC_SPC,
    _______,   XXXXXXX, XXXXXXX,  XXXXXXX,  XXXXXXX,   XXXXXXX,            XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_ENT,
                                            XXXXXXX,   _______,  XXXXXXX,  _______,  XXXXXXX
  ),
};

layer_state_t layer_state_set_user(layer_state_t state) {
  return update_tri_layer_state(state, _NUM, _SYM, _NAV);
}

